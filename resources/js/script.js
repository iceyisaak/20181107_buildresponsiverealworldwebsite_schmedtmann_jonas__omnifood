$(document).ready(function () {

    /* STICKY NAV*/
    $('.js--section-features').waypoint(function (direction) {
        if (direction == "down") {
            $('nav').addClass('sticky');
        } else {
            $('nav').removeClass('sticky');
        }
    }, {
        offset: '60px;'
    });

    /* Scroll on buttons */
    $('.js--scroll-to-plans').click(function () {
        $('html, body').animate({
                scrollTop: $('.js--section-plans').offset().top
            },
            1000
        )
    });

    $('.js--scroll-to-start').click(function () {
        $('html, body').animate({
                scrollTop: $('.js--section-features').offset().top
            },
            1000
        )
    });

    /* NAV SCROLL */
    //Works with jQuery 1.11.2//

    $(function () {
        $('a[href*=#]:not([href=#])').click(function () {
            if (location.pathname.replace(/^\//, ' ') == this.pathname.replace(/^\//, ' ') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });


    /* ANIMATION ON SCROLL */
    $('.js--waypoint-1').waypoint(function (direction) {
        $('.js--waypoint-1').addClass('animated fadeIn');
    }, {
        offset: '50%'
    });

    $('.js--waypoint-2').waypoint(function (direction) {
        $('.js--waypoint-2').addClass('animated fadeInUp');
    }, {
        offset: '50%'
    });

    $('.js--waypoint-3').waypoint(function (direction) {
        $('.js--waypoint-3').addClass('animated fadeIn');
    }, {
        offset: '50%'
    });

    $('.js--waypoint-4').waypoint(function (direction) {
        $('.js--waypoint-4').addClass('animated pulse');
    }, {
        offset: '50%'
    });

    /* MOBILE NAV */
    $('.js--nav-icon').click(function () {
        var nav = $('.js--main-nav');
        var icon = $('.js--nav-icon ion-icon');
        nav.slideToggle(200);

        if (icon.hasClass('hide')) {
            icon.toggleClass('hide')
        }

    });

    /* MAPS */

    var map = new GMaps({
        div: '.map',
        lat: 50.00,
        lng: 8.39,
        zoom: 12
    });

    map.addMarker({
        lat: 49.98172,
        lng: 8.2633813,
        title: 'Mainz',
        infoWindow: {
            content: '<p>Our EU HQ</p>'
        }
    });

});
